package com.devcamp.acountrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcountrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcountrestapiApplication.class, args);
	}

}

package com.devcamp.acountrestapi;

public class Account {
    protected String id;
    protected String name;
    protected int balance = 0;
    
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }
    public int credit (int amount){
        this.balance = this.balance + amount;
        return this.balance;
    }
    public int debit (int amount){
        if(amount <= this. balance){
            this.balance = this.balance + amount;
       
        }else{
            System.out.println("Amount exceeded balance");
        }
        return this.balance;
    }
    public int tranferTo (another a1 ,int amount){
        if(amount <= this. balance){
            this.balance = this.balance + amount;
       
        }else{
            System.out.println("Amount exceeded balance");
        }
        return this.balance;
    }
}

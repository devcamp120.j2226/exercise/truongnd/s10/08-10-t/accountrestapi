package com.devcamp.acountrestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListEmployeeItem(){
        Account taikhoan1 = new Account("12a","Tai khoan 1",10000);
        Account taikhoan2 = new Account("12b","Tai khoan 2",20000);
        Account taikhoan3 = new Account("12c","Tai khoan 3",30000);

        System.out.println(taikhoan1.toString());
        System.out.println(taikhoan2.toString());
        System.out.println(taikhoan3.toString());

        ArrayList<Account> employeeItems = new ArrayList<>();
        employeeItems.add(taikhoan1);
        employeeItems.add(taikhoan2);
        employeeItems.add(taikhoan3);

        return employeeItems;
    }
}
